<?php

use App\Http\Controllers\SearchController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource("/users", UsersController::class);

Route::get("/", function() {
    return redirect('/users');
})->name("index");

Route::get("search/{cpf}", function($cpf) {
    $queryResult = DB::table("users")->where('cpf', $cpf)->first();
    return view("search-result", ['user' => $queryResult]);
});