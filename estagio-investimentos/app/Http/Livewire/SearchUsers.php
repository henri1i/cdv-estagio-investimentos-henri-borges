<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SearchUsers extends Component
{
    public $searchQuery;
    public $currentStage;

    public function mount()
    {
        $this->searchQuery='';
        $this->currentStage='';
    }

    public function render()
    {
        return view('livewire.search-users');
    }

    public function emitSearchQuery() 
    {
        $this->emit('searchName', $this->searchQuery);
    }

    public function updatedCurrentStage() 
    {
        $this->emit('searchStage', $this->currentStage);
    }
}
