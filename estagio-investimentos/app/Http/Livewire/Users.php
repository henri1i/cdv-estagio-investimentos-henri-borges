<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Models\User;
use Livewire\WithPagination;

class Users extends Component
{
    use WithPagination;

    public $searchNameQuery;
    public $searchStageQuery;

    protected $listeners = ['searchName', 'searchStage'];

    public function mount() {
        $this->searchNameQuery = '';
        $this->searchStageQuery = '';
    }


    public function render()
    {
        if($this->searchNameQuery) {
            $users = User::where('name', 'like', `%$this->searchNameQuery`);
        }

        return view('livewire.users', [
            'users' => User::all(),
        ]);
        
    }

    public function searchName($query) {
        $this->searchNameQuery = $query;
    }

    public function searchStage($query) {
        $this->searchStageQuery = $query;
    }

    public function viewUser($userId) {
        return redirect("/users/".$userId);
    }

    public function deleteUser($userId) {
        User::find($userId)->delete();
    }
}
