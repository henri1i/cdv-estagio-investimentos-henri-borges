<?php

namespace Database\Seeders;

use App\Models\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $faker = \Faker\Factory::create();
        DB::table('users')->insert([
            'name' => $faker->name,
            'cpf' => rand(1, 99999999999),
            'current_stage' => rand(1, 3)
        ]);
    }
}
