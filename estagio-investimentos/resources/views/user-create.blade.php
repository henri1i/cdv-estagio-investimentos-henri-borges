@extends('templates.template')

@section('content')
    <header>
        @if (isset($edit))
            <a href="{{url("/users/$user->id")}}">
                <img src="{{url("/assets/icons/previous.svg")}}">
            </a>
            <h1>Atualizar</h1>
        @else
            <a href="{{url("/users")}}">
                <img src="{{url("/assets/icons/previous.svg")}}">
            </a>
            <h1>Cadastrar</h1>
        @endif
    </header>
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <p class="error">{{$error}}</p>
        @endforeach
    @endif
    <section class="content">
        @if (isset($edit))
            <form name='form' method="post" action="{{url("users/$user->id")}}">
            @method("PUT")
        @else
            <form name='form' method="post" action="{{url('users')}}">
        @endif
            @csrf
            <label for="name">Nome: </label>
            <input name='name' id="name" type="text" placeholder="Nome" autocomplete="off" value="{{$user->name??""}}" required>

            <label for="cpf">CPF: </label>
            <input name='cpf' id="cpf" type="text" placeholder="CPF" pattern="^[0-9]{3}.?[0-9]{3}.?[0-9]{3}-?[0-9]{2}"
            inputmode="number" minlength="11" maxlength="11" autocomplete="off" value="{{$user->cpf??""}}" required>

            <label for="current_stage">Etapa Atual:</label>
            <select name="current_stage" id="current_stage" required>
                @if (isset($edit))
                    @switch($user->current_stage)
                        @case(1)
                            <option value="1">Aguardando assinatura de documentos</option>
                            <option value="2">Aguardando transferência de recursos</option>
                            <option value="3">Gestão de patrimônio ativa</option>
                            @break
                        @case(2)
                            <option value="2">Aguardando transferência de recursos</option>
                            <option value="1">Aguardando assinatura de documentos</option>
                            <option value="3">Gestão de patrimônio ativa</option>
                            @break
                        @case(3)
                            <option value="3">Gestão de patrimônio ativa</option>
                            <option value="1">Aguardando assinatura de documentos</option>
                            <option value="2">Aguardando transferência de recursos</option>
                            @break    
                        @default
                            <option value="1">Aguardando assinatura de documentos</option>
                            <option value="2">Aguardando transferência de recursos</option>
                            <option value="3">Gestão de patrimônio ativa</option>
                    @endswitch
                @else
                    <option value="" disabled selected>Selecione a etapa</option>
                    <option value="1">Aguardando assinatura de documentos</option>
                    <option value="2">Aguardando transferência de recursos</option>
                    <option value="3">Gestão de patrimônio ativa</option>
                @endif
            </select>
            <div class="submit-div">
                <button id="submit" type="submit">Enviar</button>
            </div>
        </form>
    </section>
@endsection