@extends('templates.template')

@section('content')
@if ($user)
<header>
    <a href="{{$user->name}}">
        <img src="{{url("/assets/icons/previous.svg")}}">
    </a>
    <h1>{{$user->name}}</h1>
</header>
<section class="content">
    <div>
        <div id="user-divs">
            <div class="user-container">
                <p class="user-label">Nome:</p>
                <p class="user-content">{{$user->name}}</p>
            </div>
            <div class="user-container">
                <p class="user-label">CPF:</p>
                <p class="user-content">{{$user->cpf}}</p>
            </div>
            <div class="user-container">
                <p class="user-label">Etapa Atual:</p>
                @switch($user->current_stage)
                    @case(1)
                        <p class="user-content">Aguardando assinatura de documentos</p>
                        @break
                    @case(2)
                        <p class="user-content">Aguardando transferência de recursos</p>
                        @break
                    @case(3)
                        <p class="user-content">Gestão de patrimônio ativa</p>
                    @break
                    @default
                @endswitch
            </div>
        </div>
        @csrf
        <dir id="button-container">
            <form action="{{ url("/users/$user->id") }}" method="post">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button type="submit" id="destroy">Apagar</button>
            </form>
            <a href="{{url("/users/$user->id/edit")}}">
                <button id="edit">Editar</button>
            </a>
        </dir>
    </div>
</section>
@else
<header>
    <a href="{{url("users")}}">
        <img src="{{url("/assets/icons/previous.svg")}}">
    </a>
</header>
<section class="content">
    <p>Nenhum cliente possui este cpf.</p>
</section>
<a href="{{url("users")}}">
    <button id="edit">Voltar</button>
</a>
@endif
@endsection