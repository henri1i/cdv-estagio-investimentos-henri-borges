<div>
    <table>
        <tr>
            <th class="id">Id</th>
            <th>Nome</th>
            <th>Ação</th>
        </tr>
            @forelse ($users as $user)
                <tr>
                    <td class="id">{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td class="px-5 py-4 gap-4">
                        <div class="flex gap-2">
                            <div>
                                <a wire:click="viewUser({{ $user->id }})" class="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded" href="#">
                                    View
                                </a>
                            </div>
                            <div>
                                <a onclick="return confirm('Are you sure?') || event.stopImmediatePropagation()"
                                    wire:click="deleteUser({{ $user->id }})" 
                                    class="bg-red-500 hover:bg-red-400 text-white font-bold py-2 px-4 border-b-4 border-red-700 hover:border-red-500 rounded" href="#">
                                    Delete
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
            @empty
                <p>Sem registro de usuarios</p>
            @endforelse
    </table>
    {{ $users->links() }}
</div>
