<div class="flex gap-2">
    <label class="shadow text-left" style="max-width: 400px;">
        <select wire:model='currentStage' class="form-select bg-white rounded text-black w-full h-full p-2">
            <option value="">Select a stage</option>
            <option value="adelle">Aguardando assinatura de documentos</option>
            <option value="2">Aguardando transferencia de recursos</option>
            <option value="3">Gestao de patrimonio ativa</option>
        </select>
      </label>
    <div class="shadow flex">
        <input wire:model='searchQuery' wire:keydown='emitSearchQuery()' class="w-full rounded p-2 text-black" type="text" placeholder="Search...">
    </div>
</div>