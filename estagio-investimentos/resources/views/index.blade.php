@extends('templates.template')

@section('content')
    <header>
        <div>
            <h1>Clientes</h1>
            @livewire('search-users')
        </div>
    </header>
    <div class="main-container">
        <section class="content">
            @livewire('users')
        </section>
        <div id="create-div">
            <a href="{{url('/users/create')}}">
                <button id="create">Cadastrar</button>
            </a>
        </div>
    </div>
@endsection