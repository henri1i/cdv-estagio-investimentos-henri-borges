# Desafio Estágio Clube do Valor

## Como instalar e executar:

Faça o clone do repositório no local desejado: 

`git clone https://henri1i@bitbucket.org/henri1i/cdv-estagio-investimentos-henri-borges.git`

Entre na pasta raiz do projeto:

`cd cdv-estagio-investimentos-henri-borges/estagio-investimentos/`

Instale as dependencias:

`composer install`

`npm install`

Edite o arquivo .env com as configurações do banco de dados.

![](https://bitbucket.org/henri1i/cdv-estagio-investimentos-henri-borges/raw/9ed435085611c0ed0e7873a2707c7f30398333d7/estagio-investimentos/public/assets/preview/config-env.gif)

Inicie o servidor local:

`php artisan serve`

<hr>

## Funcionalidades do gerenciador de cadastro:

### Vizualizacao e edição de clientes

![](https://bitbucket.org/henri1i/cdv-estagio-investimentos-henri-borges/raw/d2410cd4b4187e3a79e51f16db80883564835b78/estagio-investimentos/public/assets/preview/visualizacao-e-edicao.gif)

### Prevenção de erros no frontend e no backend

![](https://bitbucket.org/henri1i/cdv-estagio-investimentos-henri-borges/raw/d2410cd4b4187e3a79e51f16db80883564835b78/estagio-investimentos/public/assets/preview/prevencao-de-erros.gif)

![](https://bitbucket.org/henri1i/cdv-estagio-investimentos-henri-borges/raw/d2410cd4b4187e3a79e51f16db80883564835b78/estagio-investimentos/public/assets/preview/prevencao-de-erros-backend.gif)

### Beta: Pesquisa

![](https://bitbucket.org/henri1i/cdv-estagio-investimentos-henri-borges/raw/d2410cd4b4187e3a79e51f16db80883564835b78/estagio-investimentos/public/assets/preview/pesquisa-por-cpf.gif)

<hr>

## Requisitos:

### Frontend:
- [x] Visualizar a lista de clientes cadastrados
- [x] Adicionar/Remover/Editar clientes
- [x] Atualizar etapa do cliente
- [x] Interface adaptada para dispositivos móveis

### Backend:

- [x] Dados de clientes devem ser salvo em um banco de dados
- [x] Chamada GET de api que retorna a lista de clientes e suas etapas em formato JSON.
- [x] Chamada PUT de api que atualiza a etapa de um cliente
- [x] Chamada POST de api que cria um novo cliente

<hr>

## Melhorias:

Nao consegui terminar de implementar a ferramenta de busca por cpf devido a falta de tempo, entao simulei uma barra de pesquisa na index do site, e um mecanismo de busca utilizando a url: ` http://localhost:8000/search/{cpf} `

<hr>

## Ferramentas utilizadas para a construção do projeto:

* PHP Utilizando laravel como framework.
* Eloquent para melhor e mais organizado controle da api.
* Banco de dados MySQL.